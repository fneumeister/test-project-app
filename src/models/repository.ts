export interface Repository {
  full_name: string;
  name: string;
  avatar_url: string;
  id: number;
}