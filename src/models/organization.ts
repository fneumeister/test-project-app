export interface Organization {
  login: string;
  avatar_url: string;
  id: number;
}