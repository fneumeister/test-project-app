import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { GithubRepositoriesProvider } from '../../providers/github-repositories/github-repositories';
import { GithubUsersProvider } from '../../providers/github-users/github-users';
import { Repository } from '../../models/repository';

@Component({
  selector: 'page-repos',
  templateUrl: 'repos.html'
})
export class ReposPage {
	repositories: Repository[]
  	originalRepositories: Repository[]

  constructor(public navCtrl: NavController, private githubRepositoriesProvider:GithubRepositoriesProvider,private githubUsersProvider:GithubUsersProvider) {
  	githubRepositoriesProvider.load().subscribe(repositories => {
      this.repositories = repositories;
      this.originalRepositories = repositories;
    })
  }

    search(searchEvent) {
    let term = searchEvent.target.value
    // We will only perform the search if we have 3 or more characters
    if (term.trim() === '' || term.trim().length < 3) {
      // Load cached users
      this.repositories = this.originalRepositories;
    } else {
      // Get the searched users from github
      this.githubUsersProvider.searchUsers(term).subscribe(users => {
        this.repositories = users
      });
    }
  }

  ionViewDidLoad() {
    console.log('Hello Repos Page');
  }
}