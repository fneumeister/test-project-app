import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {GithubOrganizationsProvider } from '../../providers/github-organizations/github-organizations'
import {Organization } from '../../models/organization'

@Component({
  selector: 'page-organizations',
  templateUrl: 'organisations.html'
})
export class OrganisationsPage {
	organizations: Organization[]
	originalOrganizations: Organization[]

  constructor(public navCtrl: NavController , private githubOrganizationsProvider: GithubOrganizationsProvider) {
  	githubOrganizationsProvider.load().subscribe(organizations => {
      this.organizations = organizations;
      this.originalOrganizations = organizations;
    })
  }

  search(searchEvent) {
    let term = searchEvent.target.value
    // We will only perform the search if we have 3 or more characters
    if (term.trim() === '' || term.trim().length < 3) {
      // Load cached users
      this.organizations = this.originalOrganizations;
    } else {
      // Get the searched users from github
      this.githubOrganizationsProvider.searchOrg(term).subscribe(organizations => {
        this.organizations = organizations;
      });
    }
  }

  ionViewDidLoad() {
    console.log('Hello Organisations Page');
  }
}