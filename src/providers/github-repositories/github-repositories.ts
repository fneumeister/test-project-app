import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import {Http} from '@angular/http';
import { Repository } from '../../models/repository';
/*
  Generated class for the GithubRepositoriesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GithubRepositoriesProvider {

	githubApiUrl = 'https://api.github.com';

  constructor(public http: Http) {
    console.log('Hello GithubRepositoriesProvider Provider');
  }

    // Load all github users
  load(): Observable<Repository[]> {
  	console.log('GithubRepositoriesProvider:load');
    return this.http.get(`${this.githubApiUrl}/repositories`)
      .map(res => <Repository[]>res.json());
  }

  //Search

  searchUsers(searchParam: string): Observable<Repository[]> {
    return this.http.get(`${this.githubApiUrl}/repositories?name=${searchParam}`) 
      .map(res => <Repository[]>res.json().items)
  }

}
