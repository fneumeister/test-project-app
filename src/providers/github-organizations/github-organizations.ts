import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import {Http} from '@angular/http';
import { Organization } from '../../models/organization';
/*
  Generated class for the GithubOrganizationsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GithubOrganizationsProvider {

  githubApiUrl = 'https://api.github.com';
	url = 'https://www.reddit.com/r/gifs/new/.json?limit=10';

  constructor(public http: Http) {
    console.log('Hello GithubOrganizationsProvider Provider');
  }

  load(): Observable<Organization[]> {
  	console.log('GithubOrganizationsProvider:load');
    return this.http.get(`${this.githubApiUrl}/organizations`)
      .map(res => <Organization[]>res.json());
  }

  getSome(searchParam: string) Observable<Organization[]>{
    this.http.get('${this.url}').map(res => <Organization[]>(res.json().items);
  }

  searchOrg(searchParam: string): Observable<Organization[]> {
    return this.http.get(''.${this.githubApiUrl}/organizations?since=${searchParam}.'') 
      .map(res => <Organization[]>(res.json().items))
  }

}
